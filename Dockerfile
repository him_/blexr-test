FROM php:7-fpm-alpine

RUN apk add composer php7-mysqli php7-pdo_mysql php7-common
RUN docker-php-ext-install pdo pdo_mysql
workdir /var/www/html
copy . /var/www/html
RUN composer update 
RUN chown -R www-data:www-data *