## Requirements & Info

Hi, this is my submission for the Blexr devops test, i've resolved it using 3 containers. One php fpm container for the application, one mysql container for the database and one nginx container to handle the webrequest and forward it to the php fpm container.


Please see the below steps for how to run the application locally and some explanation on the files created.


## Installation & run

Clone the repository and run

1. docker compose build
2. docker compose up -d

Now your application will be reachable on http://127.0.0.1:9000/

The database gets seeded during the compose up, if you would need to run docker compose up again it will fail and you just need to run "docker compose down" to clean up the database.

The folder is also mounted on the webserver container so you can make changes as you want to the folder/code

### Login

You can test your login with
`'test@app.com'` / `'password'` .

## Files explained
There are no comments in the files, they are only added here in the readme.

### Docker file
```
#Building from a php 7 fpm alpine image
FROM php:7-fpm-alpine

#installing the needed packages since alpine is pretty bare
RUN apk add composer php7-mysqli php7-pdo_mysql php7-common
#installing pdo mysql to have the driver for the database seeding
RUN docker-php-ext-install pdo pdo_mysql
#setting work directory and copying the repo files into the container
workdir /var/www/html
copy . /var/www/html

#installing the needed packages defined in composer.json & changing permissions for folder to permit permissions for laravel storage issues.
RUN composer update 
RUN chown -R www-data:www-data *
```
### Docker Compose file
```
# Mysql container with env variables for pw and database name.
version: '3'
services:
  mysql:
    image: mysql:5.7
    restart: 'always'
    container_name: mysql
    environment:
     - MYSQL_ROOT_PASSWORD=password
     - MYSQL_DATABASE=happs
     - MYSQL_PASSWORD=password

#Using the docker image built during composer build, linking it to the mysql container and sleeping it for 10 seconds to make sure everything is up before populating the database generating the key which gets placed in the .env file and staring php-fpm
  happs:
    depends_on:
    - mysql
    build: .
    links:
     - mysql:mysql
    container_name: happs
    command: sh -c "sleep 10; php artisan migrate --seed && php artisan key:generate && php-fpm" 
#Nginx container to take care of the web request forwarding the traffic and mounting the nginx config file from happs.conf and the code base as a volume which allows the developer to make changes to the code base from the folder.
  nginx:
    depends_on:
     - happs
    image: nginx:alpine
    container_name: happs-nginx
    restart: unless-stopped
    ports:
      - 9000:80
    volumes:
     - ./happs.conf:/etc/nginx/conf.d/default.conf
     - .:/var/www/html
```

### happs.conf
```
#forwarding traffic to the happs container, make sure that the fastcgi_pass matches container name set in the docker-compose file
server {
    listen 80;
    index index.php;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /var/www/html/public;
    location ~ \.php$ {
        try_files $uri $uri/ /index.php$is_args$args;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass happs:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
    location / {
        try_files $uri $uri/ /index.php?$query_string;
        gzip_static on;
    }
}
```

### .env file

```
#app_key will be populated during build
APP_NAME=Laravel
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

#Host name needs to match the name set inside docker compose
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=happs
DB_USERNAME=root
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

```


## Docker compose output 

```
❯ docker compose build
[+] Building 0.9s (12/12) FINISHED                                                                                                                                                                                                                                              
 => [internal] load build definition from Dockerfile                                                                                                                                                                                                                       0.0s
 => => transferring dockerfile: 32B                                                                                                                                                                                                                                        0.0s
 => [internal] load .dockerignore                                                                                                                                                                                                                                          0.0s
 => => transferring context: 2B                                                                                                                                                                                                                                            0.0s
 => [internal] load metadata for docker.io/library/php:7-fpm-alpine                                                                                                                                                                                                        0.7s
 => [1/7] FROM docker.io/library/php:7-fpm-alpine@sha256:1da273dd89ff75d55e8c248a7154860bfee5e39f31af7b362054c890e498797f                                                                                                                                                  0.0s
 => [internal] load build context                                                                                                                                                                                                                                          0.0s
 => => transferring context: 8.22kB                                                                                                                                                                                                                                        0.0s
 => CACHED [2/7] RUN apk add composer php7-mysqli php7-pdo_mysql php7-common                                                                                                                                                                                               0.0s
 => CACHED [3/7] RUN docker-php-ext-install pdo pdo_mysql                                                                                                                                                                                                                  0.0s
 => CACHED [4/7] WORKDIR /var/www/html                                                                                                                                                                                                                                     0.0s
 => CACHED [5/7] COPY . /var/www/html                                                                                                                                                                                                                                      0.0s
 => CACHED [6/7] RUN composer update                                                                                                                                                                                                                                       0.0s
 => CACHED [7/7] RUN chown -R www-data:www-data *                                                                                                                                                                                                                          0.0s
 => exporting to image                                                                                                                                                                                                                                                     0.0s
 => => exporting layers                                                                                                                                                                                                                                                    0.0s
 => => writing image sha256:53712a412d0597f63d1ef7cb675df13189b9f4ccd89889079fd98817c153bf5e                                                                                                                                                                               0.0s
 => => naming to docker.io/library/blexr-test_happs                                                                                                                                                                                                                        0.0s

Use 'docker scan' to run Snyk tests against images to find vulnerabilities and learn how to fix them
❯ docker compose up
[+] Running 4/4
 ⠿ Network "blexr-test_default"  Created                                                                                                                                                                                                                                   4.0s
 ⠿ Container mysql               Created                                                                                                                                                                                                                                   0.0s
 ⠿ Container happs               Created                                                                                                                                                                                                                                   0.0s
 ⠿ Container happs-nginx         Created                                                                                                                                                                                                                                   0.1s
Attaching to happs, happs-nginx, mysql
mysql        | 2021-07-05 08:52:54+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.34-1debian10 started.
mysql        | 2021-07-05 08:52:54+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
mysql        | 2021-07-05 08:52:54+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.34-1debian10 started.
mysql        | 2021-07-05 08:52:55+00:00 [Warn] [Entrypoint]: MYSQL_PASSWORD specified, but missing MYSQL_USER; MYSQL_PASSWORD will be ignored
mysql        | 2021-07-05 08:52:55+00:00 [Note] [Entrypoint]: Initializing database files
mysql        | 2021-07-05T08:52:55.105060Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
mysql        | 2021-07-05T08:52:55.336488Z 0 [Warning] InnoDB: New log files created, LSN=45790
mysql        | 2021-07-05T08:52:55.377916Z 0 [Warning] InnoDB: Creating foreign key constraint system tables.
mysql        | 2021-07-05T08:52:55.391859Z 0 [Warning] No existing UUID has been found, so we assume that this is the first time that this server has been started. Generating a new UUID: 63b597c6-dd6e-11eb-b4d7-0242c0a8b002.
mysql        | 2021-07-05T08:52:55.394525Z 0 [Warning] Gtid table is not ready to be used. Table 'mysql.gtid_executed' cannot be opened.
mysql        | 2021-07-05T08:52:55.879187Z 0 [Warning] CA certificate ca.pem is self signed.
mysql        | 2021-07-05T08:52:56.208820Z 1 [Warning] root@localhost is created with an empty password ! Please consider switching off the --initialize-insecure option.
mysql        | 2021-07-05 08:52:59+00:00 [Note] [Entrypoint]: Database files initialized
mysql        | 2021-07-05 08:52:59+00:00 [Note] [Entrypoint]: Starting temporary server
mysql        | 2021-07-05 08:52:59+00:00 [Note] [Entrypoint]: Waiting for server startup
mysql        | 2021-07-05T08:52:59.450968Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
mysql        | 2021-07-05T08:52:59.454771Z 0 [Note] mysqld (mysqld 5.7.34) starting as process 79 ...
mysql        | 2021-07-05T08:52:59.458281Z 0 [Note] InnoDB: PUNCH HOLE support available
mysql        | 2021-07-05T08:52:59.458364Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
mysql        | 2021-07-05T08:52:59.458424Z 0 [Note] InnoDB: Uses event mutexes
mysql        | 2021-07-05T08:52:59.458442Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
mysql        | 2021-07-05T08:52:59.458462Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
mysql        | 2021-07-05T08:52:59.458475Z 0 [Note] InnoDB: Using Linux native AIO
mysql        | 2021-07-05T08:52:59.458853Z 0 [Note] InnoDB: Number of pools: 1
mysql        | 2021-07-05T08:52:59.459012Z 0 [Note] InnoDB: Using CPU crc32 instructions
mysql        | 2021-07-05T08:52:59.461193Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
mysql        | 2021-07-05T08:52:59.469079Z 0 [Note] InnoDB: Completed initialization of buffer pool
mysql        | 2021-07-05T08:52:59.471430Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
mysql        | 2021-07-05T08:52:59.483237Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
mysql        | 2021-07-05T08:52:59.495624Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
mysql        | 2021-07-05T08:52:59.495752Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
mysql        | 2021-07-05T08:52:59.527046Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
mysql        | 2021-07-05T08:52:59.528078Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
mysql        | 2021-07-05T08:52:59.528100Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
mysql        | 2021-07-05T08:52:59.528644Z 0 [Note] InnoDB: Waiting for purge to start
mysql        | 2021-07-05T08:52:59.579170Z 0 [Note] InnoDB: 5.7.34 started; log sequence number 2747334
mysql        | 2021-07-05T08:52:59.579747Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
mysql        | 2021-07-05T08:52:59.579929Z 0 [Note] Plugin 'FEDERATED' is disabled.
mysql        | 2021-07-05T08:52:59.581752Z 0 [Note] InnoDB: Buffer pool(s) load completed at 210705  8:52:59
mysql        | 2021-07-05T08:52:59.586040Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
mysql        | 2021-07-05T08:52:59.586091Z 0 [Note] Skipping generation of SSL certificates as certificate files are present in data directory.
mysql        | 2021-07-05T08:52:59.586675Z 0 [Warning] CA certificate ca.pem is self signed.
mysql        | 2021-07-05T08:52:59.586735Z 0 [Note] Skipping generation of RSA key pair as key files are present in data directory.
mysql        | 2021-07-05T08:52:59.593492Z 0 [Warning] Insecure configuration for --pid-file: Location '/var/run/mysqld' in the path is accessible to all OS users. Consider choosing a different directory.
mysql        | 2021-07-05T08:52:59.603255Z 0 [Note] Event Scheduler: Loaded 0 events
mysql        | 2021-07-05T08:52:59.603877Z 0 [Note] mysqld: ready for connections.
mysql        | Version: '5.7.34'  socket: '/var/run/mysqld/mysqld.sock'  port: 0  MySQL Community Server (GPL)
mysql        | 2021-07-05 08:53:00+00:00 [Note] [Entrypoint]: Temporary server started.
happs-nginx  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
happs-nginx  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
happs-nginx  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
happs-nginx  | 10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
happs-nginx  | 10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf differs from the packaged version
happs-nginx  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
happs-nginx  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
happs-nginx  | /docker-entrypoint.sh: Configuration complete; ready for start up
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: using the "epoll" event method
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: nginx/1.21.0
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: built by gcc 10.2.1 20201203 (Alpine 10.2.1_pre1) 
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: OS: Linux 5.10.25-linuxkit
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: start worker processes
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: start worker process 31
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: start worker process 32
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: start worker process 33
happs-nginx  | 2021/07/05 08:53:00 [notice] 1#1: start worker process 34
mysql        | Warning: Unable to load '/usr/share/zoneinfo/iso3166.tab' as time zone. Skipping it.
mysql        | Warning: Unable to load '/usr/share/zoneinfo/leap-seconds.list' as time zone. Skipping it.
mysql        | Warning: Unable to load '/usr/share/zoneinfo/zone.tab' as time zone. Skipping it.
mysql        | Warning: Unable to load '/usr/share/zoneinfo/zone1970.tab' as time zone. Skipping it.
mysql        | 2021-07-05 08:53:03+00:00 [Note] [Entrypoint]: Creating database happs
mysql        | 
mysql        | 2021-07-05 08:53:03+00:00 [Note] [Entrypoint]: Stopping temporary server
mysql        | 2021-07-05T08:53:03.892337Z 0 [Note] Giving 0 client threads a chance to die gracefully
mysql        | 2021-07-05T08:53:03.892399Z 0 [Note] Shutting down slave threads
mysql        | 2021-07-05T08:53:03.892412Z 0 [Note] Forcefully disconnecting 0 remaining clients
mysql        | 2021-07-05T08:53:03.892426Z 0 [Note] Event Scheduler: Purging the queue. 0 events
mysql        | 2021-07-05T08:53:03.893269Z 0 [Note] Binlog end
mysql        | 2021-07-05T08:53:03.894659Z 0 [Note] Shutting down plugin 'ngram'
mysql        | 2021-07-05T08:53:03.894721Z 0 [Note] Shutting down plugin 'partition'
mysql        | 2021-07-05T08:53:03.894729Z 0 [Note] Shutting down plugin 'BLACKHOLE'
mysql        | 2021-07-05T08:53:03.894741Z 0 [Note] Shutting down plugin 'ARCHIVE'
mysql        | 2021-07-05T08:53:03.894754Z 0 [Note] Shutting down plugin 'PERFORMANCE_SCHEMA'
mysql        | 2021-07-05T08:53:03.894774Z 0 [Note] Shutting down plugin 'MRG_MYISAM'
mysql        | 2021-07-05T08:53:03.894800Z 0 [Note] Shutting down plugin 'MyISAM'
mysql        | 2021-07-05T08:53:03.894818Z 0 [Note] Shutting down plugin 'INNODB_SYS_VIRTUAL'
mysql        | 2021-07-05T08:53:03.894830Z 0 [Note] Shutting down plugin 'INNODB_SYS_DATAFILES'
mysql        | 2021-07-05T08:53:03.894841Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLESPACES'
mysql        | 2021-07-05T08:53:03.894852Z 0 [Note] Shutting down plugin 'INNODB_SYS_FOREIGN_COLS'
mysql        | 2021-07-05T08:53:03.894860Z 0 [Note] Shutting down plugin 'INNODB_SYS_FOREIGN'
mysql        | 2021-07-05T08:53:03.894869Z 0 [Note] Shutting down plugin 'INNODB_SYS_FIELDS'
mysql        | 2021-07-05T08:53:03.894901Z 0 [Note] Shutting down plugin 'INNODB_SYS_COLUMNS'
mysql        | 2021-07-05T08:53:03.894911Z 0 [Note] Shutting down plugin 'INNODB_SYS_INDEXES'
mysql        | 2021-07-05T08:53:03.894922Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLESTATS'
mysql        | 2021-07-05T08:53:03.894957Z 0 [Note] Shutting down plugin 'INNODB_SYS_TABLES'
mysql        | 2021-07-05T08:53:03.894964Z 0 [Note] Shutting down plugin 'INNODB_FT_INDEX_TABLE'
mysql        | 2021-07-05T08:53:03.894974Z 0 [Note] Shutting down plugin 'INNODB_FT_INDEX_CACHE'
mysql        | 2021-07-05T08:53:03.894983Z 0 [Note] Shutting down plugin 'INNODB_FT_CONFIG'
mysql        | 2021-07-05T08:53:03.894992Z 0 [Note] Shutting down plugin 'INNODB_FT_BEING_DELETED'
mysql        | 2021-07-05T08:53:03.895141Z 0 [Note] Shutting down plugin 'INNODB_FT_DELETED'
mysql        | 2021-07-05T08:53:03.895148Z 0 [Note] Shutting down plugin 'INNODB_FT_DEFAULT_STOPWORD'
mysql        | 2021-07-05T08:53:03.895156Z 0 [Note] Shutting down plugin 'INNODB_METRICS'
mysql        | 2021-07-05T08:53:03.895164Z 0 [Note] Shutting down plugin 'INNODB_TEMP_TABLE_INFO'
mysql        | 2021-07-05T08:53:03.895173Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_POOL_STATS'
mysql        | 2021-07-05T08:53:03.895181Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_PAGE_LRU'
mysql        | 2021-07-05T08:53:03.895190Z 0 [Note] Shutting down plugin 'INNODB_BUFFER_PAGE'
mysql        | 2021-07-05T08:53:03.895198Z 0 [Note] Shutting down plugin 'INNODB_CMP_PER_INDEX_RESET'
mysql        | 2021-07-05T08:53:03.895230Z 0 [Note] Shutting down plugin 'INNODB_CMP_PER_INDEX'
mysql        | 2021-07-05T08:53:03.895238Z 0 [Note] Shutting down plugin 'INNODB_CMPMEM_RESET'
mysql        | 2021-07-05T08:53:03.895245Z 0 [Note] Shutting down plugin 'INNODB_CMPMEM'
mysql        | 2021-07-05T08:53:03.895252Z 0 [Note] Shutting down plugin 'INNODB_CMP_RESET'
mysql        | 2021-07-05T08:53:03.895258Z 0 [Note] Shutting down plugin 'INNODB_CMP'
mysql        | 2021-07-05T08:53:03.895323Z 0 [Note] Shutting down plugin 'INNODB_LOCK_WAITS'
mysql        | 2021-07-05T08:53:03.895378Z 0 [Note] Shutting down plugin 'INNODB_LOCKS'
mysql        | 2021-07-05T08:53:03.895393Z 0 [Note] Shutting down plugin 'INNODB_TRX'
mysql        | 2021-07-05T08:53:03.895403Z 0 [Note] Shutting down plugin 'InnoDB'
mysql        | 2021-07-05T08:53:03.895454Z 0 [Note] InnoDB: FTS optimize thread exiting.
mysql        | 2021-07-05T08:53:03.896069Z 0 [Note] InnoDB: Starting shutdown...
mysql        | 2021-07-05T08:53:03.998902Z 0 [Note] InnoDB: Dumping buffer pool(s) to /var/lib/mysql/ib_buffer_pool
mysql        | 2021-07-05T08:53:04.000191Z 0 [Note] InnoDB: Buffer pool(s) dump completed at 210705  8:53:03
mysql        | 2021-07-05T08:53:05.445746Z 0 [Note] InnoDB: Shutdown completed; log sequence number 12664632
mysql        | 2021-07-05T08:53:05.451715Z 0 [Note] InnoDB: Removed temporary tablespace data file: "ibtmp1"
mysql        | 2021-07-05T08:53:05.451806Z 0 [Note] Shutting down plugin 'MEMORY'
mysql        | 2021-07-05T08:53:05.451825Z 0 [Note] Shutting down plugin 'CSV'
mysql        | 2021-07-05T08:53:05.451841Z 0 [Note] Shutting down plugin 'sha256_password'
mysql        | 2021-07-05T08:53:05.451851Z 0 [Note] Shutting down plugin 'mysql_native_password'
mysql        | 2021-07-05T08:53:05.451991Z 0 [Note] Shutting down plugin 'binlog'
mysql        | 2021-07-05T08:53:05.456004Z 0 [Note] mysqld: Shutdown complete
mysql        | 
mysql        | 2021-07-05 08:53:05+00:00 [Note] [Entrypoint]: Temporary server stopped
mysql        | 
mysql        | 2021-07-05 08:53:05+00:00 [Note] [Entrypoint]: MySQL init process done. Ready for start up.
mysql        | 
mysql        | 2021-07-05T08:53:06.076059Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server option (see documentation for more details).
mysql        | 2021-07-05T08:53:06.078762Z 0 [Note] mysqld (mysqld 5.7.34) starting as process 1 ...
mysql        | 2021-07-05T08:53:06.082178Z 0 [Note] InnoDB: PUNCH HOLE support available
mysql        | 2021-07-05T08:53:06.082253Z 0 [Note] InnoDB: Mutexes and rw_locks use GCC atomic builtins
mysql        | 2021-07-05T08:53:06.082266Z 0 [Note] InnoDB: Uses event mutexes
mysql        | 2021-07-05T08:53:06.082278Z 0 [Note] InnoDB: GCC builtin __atomic_thread_fence() is used for memory barrier
mysql        | 2021-07-05T08:53:06.082295Z 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
mysql        | 2021-07-05T08:53:06.082302Z 0 [Note] InnoDB: Using Linux native AIO
mysql        | 2021-07-05T08:53:06.083494Z 0 [Note] InnoDB: Number of pools: 1
mysql        | 2021-07-05T08:53:06.083687Z 0 [Note] InnoDB: Using CPU crc32 instructions
mysql        | 2021-07-05T08:53:06.086454Z 0 [Note] InnoDB: Initializing buffer pool, total size = 128M, instances = 1, chunk size = 128M
mysql        | 2021-07-05T08:53:06.095537Z 0 [Note] InnoDB: Completed initialization of buffer pool
mysql        | 2021-07-05T08:53:06.099201Z 0 [Note] InnoDB: If the mysqld execution user is authorized, page cleaner thread priority can be changed. See the man page of setpriority().
mysql        | 2021-07-05T08:53:06.112143Z 0 [Note] InnoDB: Highest supported file format is Barracuda.
mysql        | 2021-07-05T08:53:06.123682Z 0 [Note] InnoDB: Creating shared tablespace for temporary tables
mysql        | 2021-07-05T08:53:06.123828Z 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
mysql        | 2021-07-05T08:53:06.142966Z 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
mysql        | 2021-07-05T08:53:06.145090Z 0 [Note] InnoDB: 96 redo rollback segment(s) found. 96 redo rollback segment(s) are active.
mysql        | 2021-07-05T08:53:06.145149Z 0 [Note] InnoDB: 32 non-redo rollback segment(s) are active.
mysql        | 2021-07-05T08:53:06.146903Z 0 [Note] InnoDB: 5.7.34 started; log sequence number 12664632
mysql        | 2021-07-05T08:53:06.147533Z 0 [Note] InnoDB: Loading buffer pool(s) from /var/lib/mysql/ib_buffer_pool
mysql        | 2021-07-05T08:53:06.148093Z 0 [Note] Plugin 'FEDERATED' is disabled.
mysql        | 2021-07-05T08:53:06.153008Z 0 [Note] InnoDB: Buffer pool(s) load completed at 210705  8:53:06
mysql        | 2021-07-05T08:53:06.155474Z 0 [Note] Found ca.pem, server-cert.pem and server-key.pem in data directory. Trying to enable SSL support using them.
mysql        | 2021-07-05T08:53:06.155542Z 0 [Note] Skipping generation of SSL certificates as certificate files are present in data directory.
mysql        | 2021-07-05T08:53:06.156382Z 0 [Warning] CA certificate ca.pem is self signed.
mysql        | 2021-07-05T08:53:06.156434Z 0 [Note] Skipping generation of RSA key pair as key files are present in data directory.
mysql        | 2021-07-05T08:53:06.157382Z 0 [Note] Server hostname (bind-address): '*'; port: 3306
mysql        | 2021-07-05T08:53:06.157476Z 0 [Note] IPv6 is available.
mysql        | 2021-07-05T08:53:06.157505Z 0 [Note]   - '::' resolves to '::';
mysql        | 2021-07-05T08:53:06.157747Z 0 [Note] Server socket created on IP: '::'.
mysql        | 2021-07-05T08:53:06.161959Z 0 [Warning] Insecure configuration for --pid-file: Location '/var/run/mysqld' in the path is accessible to all OS users. Consider choosing a different directory.
mysql        | 2021-07-05T08:53:06.180434Z 0 [Note] Event Scheduler: Loaded 0 events
mysql        | 2021-07-05T08:53:06.181211Z 0 [Note] mysqld: ready for connections.
mysql        | Version: '5.7.34'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server (GPL)
happs        | Migration table created successfully.
happs        | Migrating: 2014_10_12_000000_create_users_table
happs        | Migrated:  2014_10_12_000000_create_users_table
happs        | Migrating: 2014_10_12_100000_create_password_resets_table
happs        | Migrated:  2014_10_12_100000_create_password_resets_table
happs        | Migrating: 2017_11_30_092627_create_texts_table
happs        | Migrated:  2017_11_30_092627_create_texts_table
happs        | Seeding: UsersTableSeeder
happs        | Application key [base64:VbNcEGlX23vL2vuAgTh/YJ1aniy9lGXJIyQPlE586TU=] set successfully.
happs        | [05-Jul-2021 08:53:07] NOTICE: fpm is running, pid 1
happs        | [05-Jul-2021 08:53:07] NOTICE: ready to handle connections
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:14 +0000] "GET / HTTP/1.1" 200 2604 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs        | 192.168.176.4 -  05/Jul/2021:08:53:14 +0000 "GET /index.php" 200
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:17 +0000] "GET /login HTTP/1.1" 200 4507 "http://127.0.0.1:9000/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs        | 192.168.176.4 -  05/Jul/2021:08:53:17 +0000 "GET /index.php" 200
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:17 +0000] "GET /css/app.css HTTP/1.1" 200 119930 "http://127.0.0.1:9000/login" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:17 +0000] "GET /js/app.js HTTP/1.1" 200 294973 "http://127.0.0.1:9000/login" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:21 +0000] "POST /login HTTP/1.1" 302 362 "http://127.0.0.1:9000/login" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs        | 192.168.176.4 -  05/Jul/2021:08:53:21 +0000 "POST /index.php" 302
happs-nginx  | 192.168.176.1 - - [05/Jul/2021:08:53:21 +0000] "GET /home HTTP/1.1" 200 3497 "http://127.0.0.1:9000/login" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36"
happs        | 192.168.176.4 -  05/Jul/2021:08:53:21 +0000 "GET /index.php" 200

```